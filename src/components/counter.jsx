import React, { Component } from "react";

class Counter extends Component {
  /*  state = {
    value: this.props.counter.value,
    tags: ["tag1", "tag2", "tag3"],
  };
  styles = {
    fontSize: 20,
  };

  handelIncrements = (product) => {
    // console.log(product);
    this.setState({ value: this.state.value + 1 });
  };
 */
  render() {
    return (
      <React.Fragment>
        {this.props.children}
        <span className={this.getBadgeCl()}>{this.formatvalue()}</span>
        <button
          className="btn btn-secondary btn-sm"
          onClick={() => this.props.onIncrement(this.props.counter)}
        >
          Increment
        </button>
        <button
          onClick={() => this.props.onDelete(this.props.counter.id)}
          className="btn btn-danger btn-sm m-2"
        >
          delete
        </button>
        <br></br>
        {/* <ul>
          {this.state.tags.map((tag) => (
            <li key={tag}>{tag}</li>
          ))}
        </ul> */}
      </React.Fragment>
    );
  }

  getBadgeCl() {
    let clas = "badge m-2 badge-";
    clas += this.props.counter.value === 0 ? "warning" : "primary";
    return clas;
  }

  formatvalue() {
    const { value } = this.props.counter;
    let vr = value === 0 ? "Zero" : value;
    return <h5>{vr}</h5>;
  }
}

export default Counter;
